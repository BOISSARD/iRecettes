//
//  stubdatamanager.swift
//  iRecette-Model
//
//  Created by Clément on 11/12/2018.
//  Copyright © 2018 esirem. All rights reserved.
//

import Foundation

class StubDataManager : DataManager {
    
    private var recettes : [Recette]
    
    init() {
        self.recettes = []
        let r1 = Recette(nom: "Recette1", duree: 1, budget: .reduit, difficulte: .facile, recette: "Les étapes de la recette")
        r1.ajouterIngredient(ingredient: Ingredient(nom: "Ingredient11", quantite: 11, unite: .bouteille))
        r1.ajouterIngredient(ingredient: Ingredient(nom: "Ingredient12", quantite: 12, unite: .brin))
        r1.ajouterIngredient(ingredient: Ingredient(nom: "Ingredient13", quantite: 13, unite: .cuillere))
        self.recettes.append(r1)
        
        let r2 = Recette(nom: "Recette2", duree: 2, budget: .moyen, difficulte: .moyen, recette: "Les étapes de la recette")
        r2.ajouterIngredient(ingredient: Ingredient(nom: "Ingredient21", quantite: 21, unite: .cuillereSoupe))
        r2.ajouterIngredient(ingredient: Ingredient(nom: "Ingredient22", quantite: 22, unite: .feuille))
        r2.ajouterIngredient(ingredient: Ingredient(nom: "Ingredient23", quantite: 23, unite: .g))
        r2.ajouterIngredient(ingredient: Ingredient(nom: "Ingredient24", quantite: 24, unite: .inconnu))
        self.recettes.append(r2)
    }
    
    func chargerRecettes() -> [Recette] {
        return self.recettes;
    }
    
    func sauvegarderRecettes(recettes: [Recette]) {
        
    }
    
}
