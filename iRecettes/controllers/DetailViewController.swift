//
//  DetailViewController.swift
//  iRecettes
//
//  Created by Clément on 11/12/2018.
//  Copyright © 2018 boissard.info. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!


    func configureView() {
        // Update the user interface for the detail item.
        if let recette = recetteItem {
            if let label = detailDescriptionLabel {
                label.text = recette.description
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    var recetteItem: Recette? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

