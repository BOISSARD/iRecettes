//
//  budget.swift
//  iRecette-Model
//
//  Created by esirem on 04/12/2018.
//  Copyright © 2018 esirem. All rights reserved.
//

import Foundation

enum Budget: Int {
    case inconnu = 0
    case reduit
    case moyen
    case eleve
}
