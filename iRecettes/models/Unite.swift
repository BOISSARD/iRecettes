//
//  unite.swift
//  iRecette-Model
//
//  Created by esirem on 04/12/2018.
//  Copyright © 2018 esirem. All rights reserved.
//

import Foundation

enum Unite: Int {
    
    case inconnu = 0
    case unite
    case g
    case mL
    case L
    case cuillere
    case cuillereSoupe
    case tranche
    case morceau
    case pincee
    case sachet
    case pot
    case brin
    case feuille
    case bouteille
    
}
