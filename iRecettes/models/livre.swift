//
//  livre.swift
//  iRecette-Model
//
//  Created by Clément on 11/12/2018.
//  Copyright © 2018 esirem. All rights reserved.
//

import Foundation

class Livre : CustomStringConvertible {
    
    var recettes: [Recette];
    var nbRecettes: Int {
        get {
            return self.recettes.count;
        }
    }
    
    var dataManager : DataManager?
    
    init(){
        recettes = [];
    }
    
    convenience init(dataManager : DataManager) {
        self.init()
        self.dataManager = dataManager
    }
    
    func contientRecette(_ recette: Recette) -> Bool{
        return self.recettes.contains {
            rec in
            return rec == recette;
        }
    }
    
    func getRecette(nom: String) -> (Int, Recette)? {
        for (i, r) in recettes.enumerated() {
            if r == Recette(nom: nom) {
                return (i, r);
            }
        }
        return nil;
    }
    
    func ajouterRecette(recette: Recette){
        if !self.contientRecette(recette) {
            self.recettes.append(recette);
        }
    }
    
    func ajouterRecettes(tabRecettes: [Recette]) {
        for recette in tabRecettes {
            self.ajouterRecette(recette: recette);
        }
    }
    
    func supprimerRecette(position: Int) {
        self.recettes.remove(at: position);
    }
    
    
    func supprimerRecette(nom: String) {
        let tuple = self.getRecette(nom: nom)
        if(tuple != nil){
            self.supprimerRecette(position: tuple!.0)
            //self.supprimerRecette(recette: Recette(nom: nom));
        }
    }
    
    func supprimerRecette(recette: Recette) {
        self.supprimerRecette(nom: recette.nom)
    }
    
    func modifierRecette(nomEx: String, recette: Recette) {
        let tuple = self.getRecette(nom: nomEx)
        if(tuple != nil){
            self.supprimerRecette(position: tuple!.0);
            self.recettes.insert(recette, at: tuple!.0);
        }
    }
    
    func chargerRecettes() {
        if(self.dataManager == nil) {
            
        } else {
          self.ajouterRecettes(tabRecettes: self.dataManager!.chargerRecettes())
        }
    }
    
    func sauvegarderRecettes(){
        if(self.dataManager == nil) {
            
        } else {
            self.dataManager!.sauvegarderRecettes(recettes: self.recettes)
        }
    }
    
    var description: String {
        var retour = "Livre de recette :\n==================\n";
        if (nbRecettes < 1) {
            retour += "Il n'y a aucune recette\n";
        }
        for recette in recettes {
            retour += "\(recette)\n";
        }
        return retour;
    }
}
