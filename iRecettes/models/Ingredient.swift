//
//  ingredient.swift
//  iRecette-Model
//
//  Created by esirem on 04/12/2018.
//  Copyright © 2018 esirem. All rights reserved.
//

import Foundation

class Ingredient : CustomStringConvertible {
    
    private var nom: String
    private var quantite: Int
    private var unite: Unite
    
    init(nom: String, quantite: Int, unite: Unite) {
        self.nom = nom
        self.quantite = quantite
        self.unite = unite
    }
    
    convenience init(nom: String) {
        self.init(nom: nom, quantite: 0, unite: .inconnu);
    }
    
    convenience init(){
        self.init(nom: "Défaut");
    }
    
    public var description: String {
        return "\(self.nom) : \(self.quantite) \(self.unite)"
    }
    
    public static func == (of: Ingredient, with: Ingredient) -> Bool {
        return of.nom == with.nom;
    }
}
