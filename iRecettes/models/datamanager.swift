//
//  datamanager.swift
//  iRecette-Model
//
//  Created by Clément on 11/12/2018.
//  Copyright © 2018 esirem. All rights reserved.
//

import Foundation

protocol DataManager {
    
    func chargerRecettes() -> [Recette]
    func sauvegarderRecettes(recettes : [Recette])
    
}
