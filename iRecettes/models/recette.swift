//
//  recette.swift
//  iRecette-Model
//
//  Created by esirem on 04/12/2018.
//  Copyright © 2018 esirem. All rights reserved.
//

import Foundation

class Recette : CustomStringConvertible {
    
    var nom: String;
    var recette: String;
    var duree: Int;
    var budget: Budget;
    var difficulte: Difficulte;
    var ingredients: [Ingredient];
    
    init(nom: String, duree: Int, budget: Budget, difficulte: Difficulte, recette: String) {
        self.nom = nom;
        self.duree = duree;
        self.budget = budget;
        self.difficulte = difficulte;
        self.recette = recette;
        self.ingredients = [];
    }
    
    convenience init(nom: String) {
        self.init(nom: nom, duree: 0, budget: .inconnu, difficulte: .inconnu, recette: "");
    }
    
    public func contientIngredient(_ ingredient: Ingredient) -> Bool{
        return self.ingredients.contains {
            ing in
            return ing == ingredient;
        }
    }
    
    public func ajouterIngredient(ingredient: Ingredient) {
        if !self.contientIngredient(ingredient) {
            self.ingredients.append(ingredient);
        }
    }
    
    public func supprimerIngredient(index: Int) {
        self.ingredients.remove(at: index);
    }
    
    public func supprimerIngredient(ingredient: Ingredient) {
        for (index, ing) in self.ingredients.enumerated() {
            if(ingredient == ing) {
                self.ingredients.remove(at: index);
                break;
            }
        }
    }
    
    public var description: String {
        return "\(self.nom) : \(self.duree) \(self.budget) \(self.difficulte)\n\t\(self.ingredients)\n\t\(self.recette) "
    }
    
    public static func == (of: Recette, with: Recette) -> Bool {
        return of.nom == with.nom;
    }
    
}
